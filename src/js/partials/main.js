$(".most_slider").slick({
  infinite: false,
  slidesToShow: 3,
  autoplay: false,
  slidesToScroll: 1,
  responsive: [

    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$(".most_slider_articl").slick({
  infinite: false,
  slidesToShow: 1,
  autoplay: false,
  slidesToScroll: 2,
  responsive: [

    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});




$(function () {                       //run when the DOM is ready
  $(".clickable").click(function () {  //use a class, since your ID gets mangled
    $(this).addClass("active");      //add the class to the clicked element
  });
});



$(document).ready(function () {
  $('.nice-select').niceSelect();
});


function search() {
  var searchInputValue = document.getElementById("searchInput").value;
  // Qidirish-kodlarini shu joyda yozing
}





var totalSteps = $(".steps li").length;

$(".submit").on("click", function () {
  return false;
});

$(".steps li:nth-of-type(1)").addClass("active");
$(".myContainer .form-container:nth-of-type(1)").addClass("active");

$(".form-container").on("click", ".next", function () {
  $(".steps li").eq($(this).parents(".form-container").index() + 1).addClass("active");
  $(this).parents(".form-container").removeClass("active").next().addClass("active flipInX");
});

$(".form-container").on("click", ".back", function () {
  $(".steps li").eq($(this).parents(".form-container").index() - totalSteps).removeClass("active");
  $(this).parents(".form-container").removeClass("active flipInX").prev().addClass("active flipInY");
});


/*=========================================================
*     If you won't to make steps clickable, Please comment below code 
=================================================================*/
$(".steps li").on("click", function () {
  var stepVal = $(this).find("span").text();
  $(this).prevAll().addClass("active");
  $(this).addClass("active");
  $(this).nextAll().removeClass("active");
  $(".myContainer .form-container").removeClass("active flipInX");
  $(".myContainer .form-container:nth-of-type(" + stepVal + ")").addClass("active flipInX");
});

$(function () {
  var $refreshButton = $('#refresh');
  var $results = $('#css_result');

  function refresh() {
    var css = $('style.cp-pen-styles').text();
    $results.html(css);
  }

  refresh();
  $refreshButton.click(refresh);

  // Select all the contents when clicked
  $results.click(function () {
    $(this).select();
  });
});


$(document).ready(function () {

  var current_fs, next_fs, previous_fs; //fieldsets
  var opacity;
  var current = 1;
  var steps = $("fieldset").length;

  setProgressBar(current);

  $(".next").click(function () {

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
      step: function (now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
          'display': 'none',
          'position': 'relative'
        });
        next_fs.css({ 'opacity': opacity });
      },
      duration: 500
    });
    setProgressBar(++current);
  });

  $(".previous").click(function () {

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
      step: function (now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
          'display': 'none',
          'position': 'relative'
        });
        previous_fs.css({ 'opacity': opacity });
      },
      duration: 500
    });
    setProgressBar(--current);
  });

  function setProgressBar(curStep) {
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width", percent + "%")
  }

  $(".submit").click(function () {
    return false;
  })

});


$('.show-pass').on("click", function () {
  $(".input-password").attr("type", "text");
  $(this).addClass("d-none");
  $(".hide-pass").addClass("d-block");
});

$('.hide-pass').on("click", function () {
  $(".input-password").attr("type", "password");
  $(this).removeClass("d-block");
  $(".show-pass").removeClass("d-none");
});




$('.show-pass2').on("click", function () {
  $(".input-password2").attr("type", "text");
  $(this).addClass("d-none");
  $(".hide-pass2").addClass("d-block");
});

$('.hide-pass2').on("click", function () {
  $(".input-password2").attr("type", "password");
  $(this).removeClass("d-block");
  $(".show-pass2").removeClass("d-none");
});



$(".sign-btn").on("click", function () {
  $(".login").addClass("left__")
  $(".sign-up").addClass("active")
});

$(".login_btn").on("click", function () {
  $(".login").removeClass("left__")
  $(".sign-up").removeClass("active")
});


$(".forgot__password").on("click", function () {
  $(".login").addClass("left__")
  $(".forgotpass").addClass("active")
});


$(".sign-btn").on("click", function () {
  $(".forgotpass").removeClass("active")
  $(".sign-up").addClass("active")
});

$(".login_btn").on("click", function () {
  $(".login").removeClass("left__")
  $(".forgotpass").removeClass("active")
});



$(".forgot_pass").on("click", function () {
  $(".forgotpass").removeClass("active")
  $(".forgotpass").addClass("left__")
  $(".forgot_pass_two").addClass("active")
});



$(".login_btn_one").on("click", function () {
  $(".forgotpass").removeClass("left__")
  $(".forgotpass").addClass("active")
  $(".forgot_pass_two").removeClass("active")
});

$(".sign-btn").on("click", function () {
  $(".forgotpass").removeClass("left__")
  $(".forgot_pass_two").removeClass("active")
  $(".sign-up").addClass("active")
});


$(".new_pass_btn").on("click", function () {
  $(".forgotpass").removeClass("left__")
  $(".forgot_pass_two").removeClass("active")
  $(".new_pass").addClass("active")
});

$(".sign-btn").on("click", function () {
  $(".new_pass").removeClass("active")
  $(".sign-up").addClass("active")
});

$(".last_btn").on("click", function () {
  $(".new_pass").removeClass("active")
  $(".form_check").addClass("active")

});


$(".next_test").on("click", function () {
  $(".reg_title").addClass("d-none")
  $(".test_s").addClass("d-block")
  $(".test_s").removeClass("d-none")
});


$(".back_test").on("click", function () {
  $(".reg_title").removeClass("d-none")
  $(".test_s").removeClass("d-block")
  $(".test_two").addClass("d-none")
});
$(".back_test_one").on("click", function () {
  $(".tests_s").addClass("d-none")
  $(".test_s").addClass("d-block")
  $(".test_one").addClass("d-block")
  $(".test_one").removeClass("d-none")
  $(".test_two").addClass("d-none")
  $(".test_two").removeClass("d-block")
});

$(".test_next_s").on("click", function () {
  $(".test_one").removeClass("d-block")
  $(".test_one").addClass("d-none")
  $(".test_two").removeClass("d-none")
  $(".test_two").addClass("d-block")
});

$(".test_next_s").on("click", function () {
  $(".test_s").addClass("d-none")
  $(".test_s").removeClass("d-block")
  $(".test_rezult").addClass("d-block")
  $(".test_rezult").removeClass("d-none")
});


// var player = new Plyr('.main_banner video', {
//   muted: false,
//   volume: 1,
//   controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen'],
// });

$(function () {
  $(".toggle-menu").click(function () {
    $(this).toggleClass("active");
    $('.menu-drawer').toggleClass("open");
    $('.right_blok').toggleClass("acctive_right");
  });
});

$(function () {
  $(".index_bread").click(function () {
    $(".header_bottom").toggleClass("breadgrumb_m");
  });
});
$(function () {
  $(".right_blok").click(function () {
    $(".right_blok").toggleClass("acctive_right");
    $('.menu-drawer').removeClass("open");
    $('.toggle-menu').removeClass("active");

  });
});

$(window).on('load', function () {

  $(".loader").delay(3000).fadeOut("slow");
});


const scrollContainer = document.getElementById('scrollContainer');
const scrollSpeed = 2; // Adjust the scrolling speed (lower values are slower)

function autoScroll() {
  scrollContainer.scrollLeft += scrollSpeed;
  if (scrollContainer.scrollLeft >= scrollContainer.scrollWidth - scrollContainer.clientWidth) {
    scrollContainer.scrollLeft = 0;
  }
}

// Start the auto-scrolling loop
setInterval(autoScroll, 30); // Adjust the interval (lower values are faster)


function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active_menu", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active_menu";
}